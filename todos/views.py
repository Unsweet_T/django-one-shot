from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_lst_detail = get_object_or_404(TodoList, id=id)
    context = {
        "items": todo_lst_detail,
    }
    return render(request, "todos/detail.html, context")
